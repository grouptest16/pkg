module gitlab.com/GroupTest/pkg

go 1.14

require (
	github.com/davecgh/go-spew v1.1.1 // indirect
	github.com/dghubble/oauth1 v0.6.0
	github.com/kr/pretty v0.1.0 // indirect
	github.com/pkg/errors v0.8.1
	github.com/spf13/viper v1.7.1
	github.com/stretchr/testify v1.4.0 // indirect
	go.uber.org/atomic v1.4.0 // indirect
	go.uber.org/multierr v1.1.0 // indirect
	go.uber.org/zap v1.10.0
	gopkg.in/check.v1 v1.0.0-20180628173108-788fd7840127 // indirect
	gopkg.in/yaml.v2 v2.2.4
)
