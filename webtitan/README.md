# white-titan
A command line utility that runs as Kubernetes CronJob and whitelists meraki and aruba access point IP Addresses on the webtitan platform every X minutes

## Configuration
To configure white titan you need this:

* A meraki user account
  * Generated meraki api keys
  * Retrieve meraki customer id using api
* AND/OR a aruba user account
  * Generated api keys
    * client id
    * client secret
  * the account username and password to login
  * the correct api url

* A webtitan user account
  * A webtitan customer created
  * Generated oauth keys
    * oauth consumer key
    * oauth consumer secret
    * oauth access token
    * oauth access token secret
  * Retrieve webtitan customer id using api

## Configuration File Format
These configurations for white-titan are setup in DVCP

# Running Locally (Environment Variables)

```bash
export WEBTITAN_API_BASE_URL=http://derek.webtitancloud.com:8080
export WEBTITAN_OAUTH1_CONSUMER_KEY=322b3b8792aa758c4533ae90c6fc0d62
export WEBTITAN_OAUTH1_CONSUMER_SECRET=3de5193e6a7cd35d2a8c2265427581fd
export WEBTITAN_OAUTH1_TOKEN_KEY=7d0f2b71572bc9a6538a5ba23bd8474a
export WEBTITAN_OAUTH1_TOKEN_SECRET=6d98d1229f632d358506c9327511559b
export GENESIS_HOSTNAME=localhost:5432
export GENESIS_USERNAME=genesis
export GENESIS_PASSWORD=genesis
export GENESIS_DATABASE=genesis
```

If you need to temporarily override the API endpoint you can add this structure inside of a customer in the DVCP customer entity attribute json
```json
"WEBTITAN_OAUTH":{
    "API_URL": "",
    "CONSUMER_KEY": "",
    "CONSUMER_SECRET": "",
    "TOKEN_KEY": "",
    "TOKEN_SECRET": ""
}
```

### Modify the cronjob run frequency
Changing the cronjob run frequency can be done [here](https://gitlab.dv/newtown/white-titan/blob/develop/deployments/kubetpl/CronJob.tpl.yaml)

[Cron Formatting Documentation](http://www.nncron.ru/help/EN/working/cron-format.htm)

### Add a new client
The webtitan cutomer id can only be retrieved after generating an personal access token.

## Meraki API
is an API for the web service hosted by cisco/meraki that manages physical meraki hardware access points

[Meraki Dashboard](https://n47.meraki.com)

Credentials to log in must be requested from Adam King (if you are on the Dev team) although the entire development team has permissions to create admins

You must generate a personal meraki API key to access the meraki API

Retrieve or generate personal access token:

* Top Right Corner Email Address
  * My Profile

![Meraki API Screen](readme/meraki-api-screen.png "Meraki Profile")

  * scroll down to API Access / Generate API Key
![Meraki API Generate](readme/meraki-api-generate.png "Meraki Profile Access Tokens")

### Retrieving a meraki customer id

Use the meraki api key to get the customer ids to populate development.json or production.json configs

    curl --request GET \
      --url https://api.meraki.com/api/v0/organizations/ \
      --header 'x-cisco-meraki-api-key: b7b336267f0b7acd81e97dda134b571cada03e52'
___
## Aruba API
is an API for the web service hosted by HP/Aruba that manages physical aruba hardware access points.

[Aruba Dashboard](https://portal-prod2.central.arubanetworks.com/platform/login/user)

At the time of writing this there does not exist a CIBC Aruba API which was what initiated this project.
The credentials being used are for the Starbucks Production API.

User accounts must be manually requested from Andrew Abraham of Elena Tsapkos team.
Because of the way we are doing oauth the username and password of a users account must be hardcoded in the configuration.

Retrieve or generate personal access token:
* Top Left Dropdown Menu
  * Maintanance
  ![Aruba API](readme/aruba-api.png "Aruba Menu / Maintenance")
    * API Gateway: Shows the swagger api url
  ![Aruba API Gateway](readme/aruba-api-gateway.png "Aruba Menu / Maintenance")
      * My Apps and Tokens
  ![Aruba API My Apps](readme/aruba-api-my-apps.png "Aruba Menu / Maintenance")

___
## WebTitan API
is an API for the web content filtering platform that acts as a DNS server to restrict access to prohibited domain names

[Development Web Titan Dashboard](https://derek.webtitancloud.com:8443)
[Production Web Titan Dashboard](https://datavalet.webtitancloud.com:8443)

Credentials to log in must be requested from Michael Tam, Eric Gervais, or Ryan Ferguson

The SSL Key for WebTitan can be found here https://datavalet.webtitancloud.com:8443/ssl.php

The command to convert it to a .pem file is `openssl x509 -inform der -in webtitan.der -out webtitan.pem`

The WebTitan REST API Documentation can be found here https://APIdoc.webtitancloud.com/

Ensure the API endpoint is set to `http://datavalet.webtitancloud.com:8080/` or `:8443` when making HTTP requests

Generate an API key from https://datavalet.webtitancloud.com:8443/auth-settings.php

![alt text](readme/webtitan-settings-access.png "Webtitan / Settings / Access")

## VS Code

Included in this repo is tasks to automatically test tpl configuration and build your docker file for you
Press CTRL+SHIFT+B to validate and build